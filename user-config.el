;; UC: Define executable-find function

(defun executable-find (command)
  "Search for COMMAND in `exec-path' and return the absolute file name.
  Return nil if COMMAND is not found anywhere in `exec-path'."
  ;; Use 1 rather than file-executable-p to better match the behavior of
  ;; call-process.
  (locate-file command exec-path exec-suffixes 1))

;; UC: Frame-Parameter
;;     Transparency setting

;;FRAMEISSUE    (set-frame-parameter (selected-frame) 'alpha '(85 85))
;;FRAMEISSUE    (add-to-list 'default-frame-alist '(alpha '85 85 ))

(global-set-key (kbd "s-i") 'org-indent-mode)

;; UC: Use-package

(use-package auto-package-update
  :ensure t
  :init
  (setq auto-package-update-delete-old-versions: t)
  (setq auto-package-update-hide-results nil)
  (setq auto-package-update-prompt-before-update t)
  (setq auto-package-update-update-interval 21)
  (auto-package-update-maybe)
  )

;; UC: [[http://web-mode.org/][Web-mode]]

(use-package web-mode
  :ensure t
  :defer t
  :config
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl$" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.api\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("/some/react/path/.*\\.js[x]?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode))
  (add-to-list 'auto-mode-alist '("\\.blade\\.php\\'" . web-mode))
  ;; Customization
  (defun my-web-mode-hook ()
    "Hooks for Web mode."
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-enable-auto-pairing nil)
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-css-indent-offset 2)
    (setq web-mode-code-indent-offset 2)
    (setq web-mode-enable-auto-closing t)
    ;;      (setq web-mode-content-types-alist
    ;;            ("json" . "/some/path/.*\\.api\\'")
    ;;            ("xml"  . "/other/path/.*\\.api\\'")
    ;;            ("jsx"  . "/some/react/path/.*\\.js[x]?\\'"))
    (setq web-mode-enable-current-element-highlight t))
  ;;ISSUE_I_THINK    (add-hook 'web-mode-hook  'my-web-mode-hook)
  ;; You can disable arguments|concatenation|calls lineup with
  (add-to-list 'web-mode-indentation-params '("lineup-args" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-calls" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-concats" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-ternary" . nil))
  ;; Autoremove final white spaces on save
  (add-hook 'local-write-file-hooks
            (lambda ()
              (delete-trailing-whitespace)
              nil))
  (defun sp-web-mode-is-code-context (id action context)
    (and (eq action 'insert)
         (not (or (get-text-property (point) 'part-side)
                  (get-text-property (point) 'block-side)))))
  ;;GERRY    (sp-local-pair 'web-mode "<" nil :when '(sp-web-mode-is-code-context))
  (setq web-mode-enable-current-element-highlight t)
  (setq web-mode-ac-sources-alist
        '(("css" . (ac-source-css-property))
          ("html" . (ac-source-words-in-buffer ac-source-abbrev))))
  )
(eval-after-load "web-mode"
  '(set-face-background 'web-mode-current-element-highlight-face "green4"))

;; UC: Ace windows for easy window switching

(use-package ace-window
  :ensure t
  :init
  (progn
    (setq aw-scope 'frame)
    (global-set-key (kbd "C-x O") 'other-frame)
    (global-set-key [remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))
    ))

;; UC: Emacs Calendar
;;     See: [[https://www.emacswiki.org/emacs/InsertingTodaysDate][Inserting Today’s Calendar]]
;;     You can run this command with ‘M-x insdate-insert-current-date’ or bind this command to ‘C-x M-d’:
;;     # (global-set-key "\C-x\M-d" `insdate-insert-current-date)

(use-package calendar
  :ensure t)
(defun insdate-insert-current-date (&optional omit-day-of-week-p)
  "Insert today's date using the current locale.
    With a prefix argument, the date is inserted without the day of
    the week."
  (interactive "P*")
  (insert (calendar-date-string (calendar-current-date) nil
                                omit-day-of-week-p)))
(global-set-key "\C-x\M-d" `insdate-insert-current-date)

;; UC: Wunderlist (disabled - Wunderlist has shutdown)



;; UC: Save point position between sessions

;; Save point position between sessions
(use-package saveplace
  :ensure t
  :init
  (setq-default save-place t)
  (setq save-place-file (expand-file-name ".places" user-emacs-directory)))

;; UC: Are we on a mac?

(setq is-mac (equal system-type 'darwin))

;; UC: org-mode key-bindings

;; Custom Key Bindings
(define-key input-decode-map "\C-i" [C-i])
(global-set-key (kbd "<f12>") 'org-agenda)
(global-set-key (kbd "<f5>") 'bh/org-todo)
(global-set-key (kbd "<S-f5>") 'bh/widen)
(global-set-key (kbd "<f7>") 'bh/set-truncate-lines)
(global-set-key (kbd "<S-f8>") 'org-cycle-agenda-files)
(global-set-key (kbd "<f9> <f9>") 'bh/show-org-agenda)
(global-set-key (kbd "<f9> b") 'bbdb)
(global-set-key (kbd "<f9> c") 'calendar)
(global-set-key (kbd "<f9> f") 'boxquote-insert-file)
(global-set-key (kbd "<f9> g") 'gnus)
(global-set-key (kbd "<f9> h") 'bh/hide-other)
(global-set-key (kbd "<f9> n") 'bh/toggle-next-task-display)
(global-set-key (kbd "<f9> I") 'bh/punch-in)
(global-set-key (kbd "<f9> O") 'bh/punch-out)
(global-set-key (kbd "<f9> o") 'bh/make-org-scratch)
(global-set-key (kbd "<f9> r") 'boxquote-region)
(global-set-key (kbd "<f9> s") 'bh/switch-to-scratch)
(global-set-key (kbd "<f9> t") 'bh/insert-inactive-timestamp)
(global-set-key (kbd "<f9> T") 'bh/toggle-insert-inactive-timestamp)
(global-set-key (kbd "<f9> v") 'visible-mode)
(global-set-key (kbd "<f9> l") 'org-toggle-link-display)
(global-set-key (kbd "<f9> SPC") 'bh/clock-in-last-task)
(global-set-key (kbd "C-<f9>") 'previous-buffer)
(global-set-key (kbd "M-<f9>") 'org-toggle-inline-images)
(global-set-key (kbd "C-x n r") 'narrow-to-region)
(global-set-key (kbd "C-<f10>") 'next-buffer)
(global-set-key (kbd "C-c C-x C-i") 'org-clock-goto)
(global-set-key (kbd "<f11>") 'org-clock-goto)
(global-set-key (kbd "C-<f11>") 'org-clock-in)
(global-set-key (kbd "C-s-<f12>") 'bh/save-then-publish)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c d")
                (lambda () (interactive) (find-file "~/Dropbox/MyLifeInOrg/gtd/DailyPlan.org")))
(global-set-key (kbd "C-c e")
                (lambda () (interactive) (find-file "~/Programming/learning/go/udemy/Learn_How_To_Code_Googles_Go.org")))
(defun bh/hide-other ()
  (interactive)
  (save-excursion
    (org-back-to-heading 'invisible-ok)
    (hide-other)
    (org-cycle)
    (org-cycle)
    (org-cycle)))
(defun bh/set-truncate-lines ()
  "Toggle value of truncate-lines and refresh window display."
  (interactive)
  (setq truncate-lines (not truncate-lines))
  ;; now refresh window display (an idiom from simple.el):
  (save-excursion
    (set-window-start (selected-window)
                      (window-start (selected-window)))))
(defun bh/make-org-scratch ()
  (interactive)
  (find-file "/tmp/publish/scratch.org")
  (gnus-make-directory "/tmp/publish"))
(defun bh/switch-to-scratch ()
  (interactive)
  (switch-to-buffer "*scratch*")
  )

;; UC: Reveal.js

;; Reveal Mode line support
(setq spaceline-org-clock-p t)
;;reveal
(setq org-reveal-mathjax t)
(use-package ox-reveal
  :ensure ox-reveal)
(setq org-reveal-location "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
(setq org-reveal-root "file://~/Programming/git/bitbucket.org/drgeb/repos/development/reveal.js/js/reveal.js")

;; UC: Syntax highlighting in codeblocks
;;     [[https://gitlab.com/Kaligule/emacs-config/-/blob/master/config.org][Emacs Config ]]

(use-package htmlize :ensure t)
(setq org-latex-listings 'minted)
(require 'ox-latex)
(add-to-list 'org-latex-packages-alist '("" "minted"))

;; UC: ess

;; What is ESS: It is designed to support editing of scripts and interaction with various statistical analysis programs such as R, S-Plus, SAS, Stata and OpenBUGS/JAGS.
(use-package ess
  :ensure t
  )

;; UC: Icomplete-mode
;;   ;; See [[https://www.emacswiki.org/emacs/IcompleteMode][IcompleteMode]]

(icomplete-mode 1)

;; UC: Smartparens

(use-package smartparens
   :ensure t
   :config
(use-package smartparens-config)
   (use-package smartparens-html)
   (use-package smartparens-python)
   (use-package smartparens-latex)
   (smartparens-global-mode t)
   (show-smartparens-global-mode t)
   :bind
   ( ("C-<down>" . sp-down-sexp)
     ("C-<up>"   . sp-up-sexp)
     ("M-<down>" . sp-backward-down-sexp)
     ("M-<up>"   . sp-backward-up-sexp)
     ("C-M-a" . sp-beginning-of-sexp)
     ("C-M-e" . sp-end-of-sexp)

     ("C-M-f" . sp-forward-sexp)
     ("C-M-b" . sp-backward-sexp)

     ("C-M-n" . sp-next-sexp)
     ("C-M-p" . sp-previous-sexp)

     ("C-S-f" . sp-forward-symbol)
     ("C-S-b" . sp-backward-symbol)

     ("C-<right>" . sp-forward-slurp-sexp)
     ("M-<right>" . sp-forward-barf-sexp)
     ("C-<left>"  . sp-backward-slurp-sexp)
     ("M-<left>"  . sp-backward-barf-sexp)

     ("C-M-t" . sp-transpose-sexp)
     ("C-M-k" . sp-kill-sexp)
     ("C-k"   . sp-kill-hybrid-sexp)
     ("M-k"   . sp-backward-kill-sexp)
     ("C-M-w" . sp-copy-sexp)

     ("C-M-d" . delete-sexp)

     ("M-<backspace>" . backward-kill-word)
     ("C-<backspace>" . sp-backward-kill-word)
     ([remap sp-backward-kill-word] . backward-kill-word)

     ("M-[" . sp-backward-unwrap-sexp)
     ("M-]" . sp-unwrap-sexp)

     ("C-x C-t" . sp-transpose-hybrid-sexp)

     ("C-c ("  . wrap-with-parens)
     ("C-c ["  . wrap-with-brackets)
     ("C-c {"  . wrap-with-braces)
     ("C-c '"  . wrap-with-single-quotes)
     ("C-c \"" . wrap-with-double-quotes)
     ("C-c _"  . wrap-with-underscores)
     ("C-c `"  . wrap-with-back-quotes)
     ))

 ;;  (use-package smartparens)
 ;;  (use-package smartparens-config)
 ;;       (setq sp-autoescape-string-quote nil)
 ;;      (--each '(css-mode-hook
 ;;                 restclient-mode-hook
 ;;                 js-mode-hook
 ;;                 java-mode
 ;;                 ruby-mode
 ;;                 markdown-mode
 ;;                 groovy-mode
 ;;                 scala-mode)
 ;;         (add-hook it 'turn-on-smartparens-mode))

;; UC: UTF8



;; UC: Ensime (This has been abandoned)

;;       (use-package ensime
;;         :ensure t
;;         :defer f
;;         )

;; UC: Ok here we begin



;; UC: Treemacs file browser
;;   See: [[http://cestlaz.github.io/posts/using-emacs-37-treemacs/#.Wci9s9OGNQ0][Using Emacs 37 - Treemacs file browser]]

(message "drgeb UC: Treemacs file browser")
              (use-package treemacs
                :ensure t
                :defer t
;;      :config
;;      (progn
;;        (use-package treemacs-evil
;;          :ensure t
;;          :demand t)
;;        (setq treemacs-follow-after-init          t
;;              treemacs-width                      35
;;              treemacs-indentation                5
;;              treemacs-git-integration            t
;;              treemacs-collapse-dirs              3
;;              treemacs-silent-refresh             nil
;;              treemacs-change-root-without-asking nil
;;              treemacs-sorting                    'alphabetic-desc
;;              treemacs-show-hidden-files          t
;;              treemacs-never-persist              nil
;;              treemacs-is-never-other-window      nil
;;              treemacs-goto-tag-strategy          'refetch-index)
;;        (treemacs-follow-mode t)
;;        (treemacs-filewatch-mode t))
;;      :bind
;;      (:map global-map
;;            ([f8]        . treemacs-toggle)
;;            ("M-0"       . treemacs-select-window)
;;            ("C-c 1"     . treemacs-delete-other-windows)
;;            ("C-c ft"    . treemacs-toggle)
;;            ("C-c fT"    . treemacs)
;;            ("C-c f C-t" . treemacs-find-file)))
    )
    (use-package treemacs-projectile
      :defer t
      :ensure t
                :config
                (setq treemacs-header-function #'treemacs-projectile-create-header)
                :bind (:map global-map
                            ("C-c fP" . treemacs-projectile)
                            ("C-c fp" . treemacs-projectile-toggle)))
    (defun treemacs--read-persist-lines (&optional txt)
      (-some->> (or txt (when (file-exists-p treemacs-persist-file)
                          (f-read treemacs-persist-file)))
        (s-trim)
        (s-lines)
        (--reject (or (s-blank-str? it)
                      (s-starts-with? "#" it)))))

;; UC: pdf-tools

(with-eval-after-load 'pdf-tools
  (setq pdf-tools-handle-upgrades nil) ; Use brew upgrade pdf-tools instead.
  )

;; UC: OSX LOCATION

(with-eval-after-load 'osx-location
  '(when (eq system-type 'darwin)
     (add-hook 'osx-location-changed-hook
               (lambda ()
                 (setq calendar-latitude osx-location-latitude
                       calendar-longitude osx-location-longitude
                       calendar-location-name (format "%s, %s" osx-location-latitude osx-location-longitude))))))

;; UC: OPAM

; Add opam emacs directory to your load-path by appending this to your .emacs:
(let ((opam-share (ignore-errors (car (process-lines \"opam\" \"config\" \"var\" \"share\")))))
  (when (and opam-share (file-directory-p opam-share))
    ;; Register Merlin
    (add-to-list 'load-path (expand-file-name \"dotspacemacs/site-lisp\" opam-share))
    (autoload 'merlin-mode \"merlin\" nil t nil)
    ;; Automatically start it in OCaml buffers
    (add-hook 'tuareg-mode-hook 'merlin-mode t)
    (add-hook 'caml-mode-hook 'merlin-mode t)
    ;; Use opam switch to lookup ocamlmerlin binary
    (setq merlin-command 'opam)))

;; UC: keys



;; UC: magit
;;     Key bindings for magit
;;     See: [[https://www.masteringemacs.org/article/introduction-magit-emacs-mode-git][Introduction Magit emacs mode git]]
;;     See: [[https://www.emacswiki.org/emacs/Magit][Emacs Wiki]]

(use-package magit
  :ensure t
  :init
  (progn
    (bind-key "C-x g" 'magit-status)
    (bind-key "C-x M-g" 'magit-dispatch-popup))
  :commands magit-status
  :diminish magit-auto-revert-mode
  :config
  (progn
    (setq magit-completing-read-function 'ivy-completing-read)
    (setq magit-item-highlight-face 'bold)
    (setq magit-repo-dirs-depth 1)
    ))

(use-package git-gutter
  :ensure t
  :config
  (global-git-gutter-mode +1)
  ;; If you would like to use git-gutter.el and linum-mode
  (git-gutter:linum-setup)
  ;; If you enable git-gutter-mode for some modes
    (add-hook 'ruby-mode-hook 'git-gutter-mode)
  (global-set-key (kbd "C-x C-g") 'git-gutter)
  (global-set-key (kbd "C-x v =") 'git-gutter:popup-hunk)
  ;; Jump to next/previous hunk
  (global-set-key (kbd "C-x p") 'git-gutter:previous-hunk)
  (global-set-key (kbd "C-x n") 'git-gutter:next-hunk)
  ;; Stage current hunk
  (global-set-key (kbd "C-x v s") 'git-gutter:stage-hunk)
  ;; Revert current hunk
  (global-set-key (kbd "C-x v r") 'git-gutter:revert-hunk)
  ;; Mark current hunk
  (global-set-key (kbd "C-x v SPC") #'git-gutter:mark-hunk)
  )
(global-set-key (kbd "M-g M-g") 'hydra-git-gutter/body)
(use-package git-timemachine
  :ensure t
  )
(defhydra hydra-git-gutter (:body-pre (git-gutter-mode 1)
                                      :hint nil)
  "
Git gutter:
  _j_: next hunk        _s_tage hunk     _q_uit
  _k_: previous hunk    _r_evert hunk    _Q_uit and deactivate git-gutter
  ^ ^                   _p_opup hunk
  _h_: first hunk
  _l_: last hunk        set start _R_evision
"
  ("j" git-gutter:next-hunk)
  ("k" git-gutter:previous-hunk)
  ("h" (progn (goto-char (point-min))
              (git-gutter:next-hunk 1)))
  ("l" (progn (goto-char (point-min))
              (git-gutter:previous-hunk 1)))
  ("s" git-gutter:stage-hunk)
  ("r" git-gutter:revert-hunk)
  ("p" git-gutter:popup-hunk)
  ("R" git-gutter:set-start-revision)
  ("q" nil :color blue)
  ("Q" (progn (git-gutter-mode -1)
              ;; git-gutter-fringe doesn't seem to
              ;; clear the markup right away
              (sit-for 0.1)
              (git-gutter:clear))
   :color blue))

;;  (global-set-key (kbd "C-x g") 'magit-status)
;;    (global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)

;; UC: font scaling

(global-set-key (kbd "C-M-=") 'text-scale-increase)
(global-set-key (kbd "C-M--") 'text-scale-decrease)

;; UC: buffer-kill-current
;;   Kill current buffers.

(global-set-key (kbd "C-x k") 'spacemacs/kill-this-buffer)

;; UC: buffer-reload

(global-set-key (kbd "C-x C-v") 'spacemacs/safe-revert-buffer)

;; UC: elfeed

(global-set-key (kbd "C-M-x w") 'elfeed)

;; UC: Auto Mode Alist



;; UC: Smarty
;;   handle files ending in '.tpl'.

(add-to-list 'auto-mode-alist '("\\.tpl$" . web-mode))

;; UC: Env
;;   handle files ending in '.env'.

(add-to-list 'auto-mode-alist '("\\.env$" . conf-mode))

;; UC: flowconfig
;;   Handle flowconfig `.flowconfig`.

(add-to-list 'auto-mode-alist '("\\.flowconfig$" . conf-mode))

;; UC: JSX JavaScript - ReactMode

;; https://github.com/felipeochoa/rjsx-mode
(add-to-list 'auto-mode-alist '("components\\/.*\\.js\\'" . rjsx-mode))
;;this is what I had before! (add-to-list 'auto-mode-alist '("\\.jsx?$" . react-mode))

;; UC: objc

(add-to-list 'auto-mode-alist '("\\.m$" . objc-mode))

;; UC: EditorConfig
;;   Always enable EditorConfig.

(editorconfig-mode 1)

;; UC: regexp
;;   I prefer to use PCRE REGEX engine instead.

(pcre-mode)
'(vr/engine (quote pcre2el))

;; UC: Origami
;;   Enable Origami globally

(global-origami-mode t)

;; UC: PowerLine
;;   Set `Powerline' default separator to arrow.

(use-package powerline
  :ensure t
  :config
  (setq powerline-default-separator 'bar)
    )

;; UC: Spaceline

;; Spaceline
(setq powerline-default-separator 'arrow
      spaceline-buffer-encoding-abbrev-p nil
      spaceline-version-control-p nil
      spaceline-erc-track-p nil)

;; UC: Sunshine (weather forecast)
;;   Switch to metric.

(setq sunshine-units 'metric)


;; Weather forecast icons can be toggled by pressing `i' within this mode's main
;; buffer.

(setq sunshine-show-icons t)

;; UC: NeoTree (neotree substituted for tree
;;   Set the NeoTree Theme to Nerd

;; (setq neo-theme 'nerd)

;; UC: Skeletor
;;   =Skeletor= provides project templates for Emacs. It also automates the mundane
;;   parts of setting up a new project like version control, licenses and tooling.
;;   =Skeletor= comes with a number of predefined templates and allows you to easily
;;   create your own.

(setq skeletor-project-directory "~/.spacemacs.d/project-skeletons")

;; UC: Deft

(setq deft-directory org-directory)

;; UC: Projectile

;; projectile
  (use-package projectile
    :ensure t
    :config
    (projectile-global-mode)
    (setq projectile-completion-system 'ivy))
  (use-package counsel-projectile
    :ensure t
    :config
    (counsel-projectile-mode)
)



;; Once you have selected your project, the top-level directory
;; of the project is immediately opened for you in a dired buffer.

(setq projectile-switch-project-action 'projectile-dired)



;; Make projectile work with tramp mode.

(defadvice projectile-project-root (around ignore-remote first activate)
  (unless (file-remote-p default-directory) ad-do-it))

;; UC: windmove
;;   Windmove lets you move point from window to window using Shift and the arrow keys.

(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))



;; Make windmove work in org-mode.

(add-hook 'org-shiftup-final-hook 'windmove-up)
(add-hook 'org-shiftleft-final-hook 'windmove-left)
(add-hook 'org-shiftdown-final-hook 'windmove-down)
(add-hook 'org-shiftright-final-hook 'windmove-right)

;; UC: elfeed-org

(setq elfeed-db-directory "~/.elfeed")
     (defun elfeed-mark-all-as-read ()
       (interactive)
       (mark-whole-buffer)
       (elfeed-search-untag-all-unread))
     ;;functions to support syncing .elfeed between machines
     ;;makes sure elfeed reads index from disk before launching
     (defun bjm/elfeed-load-db-and-open ()
       "Wrapper to load the elfeed db from disk before opening"
       (interactive)
       (elfeed-db-load)
       (elfeed)
       (elfeed-search-update--force))
     ;;write to disk when quiting
     (defun bjm/elfeed-save-db-and-bury ()
       "Wrapper to save the elfeed db to disk before burying buffer"
       (interactive)
       (elfeed-db-save)
       (quit-window))
     (use-package elfeed
       :ensure t
       :bind (:map elfeed-search-mode-map
                   ("q" . bjm/elfeed-save-db-and-bury)
                   ("Q" . bjm/elfeed-save-db-and-bury)
                   ("m" . elfeed-toggle-star)
                   ("M" . elfeed-toggle-star)
                   ("j" . mz/make-and-run-elfeed-hydra)
                   ("J" . mz/make-and-run-elfeed-hydra))
       :config
       (defalias 'elfeed-toggle-star
         (elfeed-expose #'elfeed-search-toggle-all 'star)))
     (use-package elfeed-org
       :ensure t
       :config
       (elfeed-org)
       (setq rmh-elfeed-org-files (list (concat dotspacemacs-directory "private/elfeed1.org")
                                        (concat dotspacemacs-directory "private/elfeed2.org"))))
     (defun z/hasCap (s) ""
            (let ((case-fold-search nil))
              (string-match-p "[[:upper:]]" s)))
     (defun z/get-hydra-option-key (s)
       "returns single upper case letter (converted to lower) or first"
       (interactive)
       (let ( (loc (z/hasCap s)))
         (if loc
             (downcase (substring s loc (+ loc 1)))
           (substring s 0 1)
           )))
     ;;  (active blogs cs eDucation emacs local misc sports star tech unread webcomics)
     (defun mz/make-elfeed-cats (tags)
       "Returns a list of lists. Each one is line for the hydra configuratio in the form
          (c function hint)"
       (interactive)
       (mapcar (lambda (tag)
                 (let* (
                        (tagstring (symbol-name tag))
                        (c (z/get-hydra-option-key tagstring))
                        )
                   (list c (append '(elfeed-search-set-filter) (list (format "@6-months-ago +%s" tagstring) ))tagstring  )))
               tags))
     (defmacro mz/make-elfeed-hydra ()
       `(defhydra mz/hydra-elfeed ()
          "filter"
          ,@(mz/make-elfeed-cats (elfeed-db-get-all-tags))
          ("*" (elfeed-search-set-filter "@6-months-ago +star") "Starred")
          ("M" elfeed-toggle-star "Mark")
          ("A" (elfeed-search-set-filter "@6-months-ago") "All")
          ("T" (elfeed-search-set-filter "@1-day-ago") "Today")
          ("Q" bjm/elfeed-save-db-and-bury "Quit Elfeed" :color blue)
          ("q" nil "quit" :color blue)
          ))
     (defun mz/make-and-run-elfeed-hydra ()
       ""
       (interactive)
       (mz/make-elfeed-hydra)
       (mz/hydra-elfeed/body))
(message "drgeb loaded and configured: UC elfeed")

;; UC: Private File

(if (boundp 'dotspacemacs-private-file)
    (load-file dotspacemacs-private-file))

;; UC: Org Hotfix



;; UC: Typescript

(use-package ob-typescript)

;; UC: Setup org-gcal

(use-package org-gcal
  :ensure t
  :config
  (setq org-gcal-client-id "1034423519169-rsrc9rt1u265p0k86ttvl6aqli37h44a.apps.googleusercontent.com"
        org-gcal-client-secret "nYaJxg5Mkb4RPbDLV4Xl-bij"
        org-gcal-file-alist '(("drgbennett@gmail.com" .  "~/Dropbox/MyLifeInOrg/gcal.org"))))
(add-hook 'org-agenda-mode-hook (lambda () (org-gcal-sync) ))
(add-hook 'org-capture-after-finalize-hook (lambda () (org-gcal-sync) ))

;; UC: Setup org-sql

(use-package sql
  :ensure t
  )

;; UC: Setup evil-matchit Navigate/Select html tags in Emacs
;;   See: [[http://blog.binchen.org/posts/navigateselect-html-tags-in-emacs.html][]]

;;UNAVAILABLE  (require 'evil-matchit)
;;UNAVAILABLE  (global-evil-matchit-mode 1)
                                        ;gg  (eval-after-load 'evil-matchit-ruby
                                        ;gg    '(progn
                                        ;gg      (add-to-list 'evilmi-ruby-extract-keyword-howtos '("^[ \t]*\\([a-z]+\\)\\( .*\\| *\\)$" 1))
                                        ;gg       (add-to-list 'evilmi-ruby-match-tags '(("unless" "if") ("elsif" "else") "end"))
                                        ;gg       ))
                                        ; Support more major modes
                                        ;g  (plist-put evilmi-plugins my-mode '((evilmi-simple-get-tag evilmi-simple-jump)
                                        ;g                                      (evilmi-html-get-tag evilmi-html-jump)))
                                        ; Mixed languages in one html template file is supported
                                        ;gg (plist-put evilmi-plugins web-mode
                                        ;gg            '((evilmi-python-get-tag evilmi-python-jump)
                                        ;gg              (evilmi-html-get-tag evilmi-html-jump)
                                        ;gg              ))
                                        ; Jump between the two end of the “string”
;;UNAVAILABLE  (setq evilmi-quote-chars (string-to-list "'\"/"))

;; UC: Setup org-agenda

(setq org-refile-targets '((org-agenda-files :maxlevel . 5)))
(setq load-path (append (list (expand-file-name "/usr/share/emacs/site-lisp/org")) load-path))
:config
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
;;  (global-set-key "\C-cb" 'org-icomplete)
(global-set-key "\C-cb" 'org-iswitchb)
(global-set-key (kbd "<f6>") 'org-capture)
(setq org-enforce-todo-dependencies t)
(setq org-track-ordered-property-with-tag t)
(setq org-agenda-dim-blocked-tasks t)
(setq org-enforce-todo-checkbox-dependencies t)

;; UC: EBNF

;; EBNF grammar
(define-generic-mode 'ebnf-mode
  '("#" ("(*" . "*)"))
  '()
  '(("^[^ \t\n][^=]+" . font-lock-variable-name-face)
    ("['\"].*?['\"]" . font-lock-string-face)
    ("/.*/" . font-lock-string-face)
    ("=" . font-lock-keyword-face)
    ("@\\+?:" . font-lock-keyword-face)
    ("\\$" . font-lock-keyword-face)
    ("\\?.*\\?" . font-lock-negation-char-face)
    ("\\[\\|\\]\\|{\\|}\\|(\\|)\\||\\|,\\|;" . font-lock-type-face))
  '("\\.ebnf\\'")
  `(,(lambda () (setq mode-name "EBNF")))
  "Major mode for EBNF metasyntax text highlighting.")

;; UC: JDE

;;(add-to-list 'load-path (format "%s/dist/jdee-2.4.1/lisp" my-jdee-path))
                                        ;g  (autoload 'jde-mode "jde" "JDE mode" t)
                                        ;g  (setq auto-mode-alist
                                        ;g        (append '(("\\.java\\'" . jde-mode)) auto-mode-alist))
                                        ;g  (setq jde-web-browser "firefox")
                                        ;g  (setq jde-doc-dir "~/Programming/java_apps/jdk-8u144-docs-all")
                                        ;g  (custom-set-variables
                                        ;g   '(jdee-server-dir "~/Programming/git/bitbucket.org/drgeb/repos/emacs/packages/jdee-server/target")
                                        ;g   )

;; UC: Save Cursor Position

;; remember cursor position, for emacs 25.1 or later
(save-place-mode 1)
(message "drgeb loaded and configured: Save Cursor Position")

;; UC: display “lambda” as “λ”

(global-prettify-symbols-mode 1)

;; UC: Adding Support for Pretty Lambda Mode

(setq prettify-symbols-alist '(("lambda" . 955)))

;; UC: Prettify symbols

(setq prettify-symbols-alist
      '(
        ("lambda" . 955) ; λ
        ("->" . 8594)    ; →
        ("=>" . 8658)    ; ⇒
        ("map" . 8614)   ; ↦
        ))
(defun my-add-pretty-lambda ()
  "make some word or string show as pretty Unicode symbols"
  (setq prettify-symbols-alist
        '(
          ("lambda" . 955) ; λ
          ("->" . 8594)    ; →
          ("=>" . 8658)    ; ⇒
          ("map" . 8614)   ; ↦
          )))
(add-hook 'clojure-mode-hook 'my-add-pretty-lambda)
(add-hook 'haskell-mode-hook 'my-add-pretty-lambda)
(add-hook 'shen-mode-hook 'my-add-pretty-lambda)
(add-hook 'tex-mode-hook 'my-add-pretty-lambda)

;; UC: [[https://github.com/syohex/emacs-git-gutter][GitGutter]]

;; Live updating
;; '(git-gutter:update-interval 2)
;;   '(git-gutter:modified-sign " ") ;; two space
;;   '(git-gutter:added-sign "++")    ;; multiple character is OK
;; '(git-gutter:deleted-sign "--")
;;GEB (set-face-background 'git-gutter:modified "purple") ;; background color
;;GEB (set-face-foreground 'git-gutter:added "green")
;;GEB (set-face-foreground 'git-gutter:deleted "red")
;; first character should be a space
;;GEB  '(git-gutter:lighter " GG")
;;GEB '(git-gutter:update-interval 2)
;;GEB )
(setq git-gutter+-window-width 2)
(setq git-gutter+-modified-sign "☁")
(setq git-gutter+-added-sign "☀")
(setq git-gutter+-deleted-sign "☂")

;; UC: Multiple Cursors

(use-package multiple-cursors
  :ensure t
  :config
  (global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines) ;;Control-Shift-c Control-Shift-c
  (global-set-key (kbd "C->") 'mc/mark-next-like-this) ;;Control-. Control-Shift-.
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
  (global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click))

;; UC: Avy

(global-set-key (kbd "C-:") 'avy-goto-char)
(global-set-key (kbd "M-g f") 'avy-goto-line)
(global-set-key (kbd "M-g e") 'avy-goto-word-0)

;; UC: [[https://www.emacswiki.org/emacs/WindMove][WinMove]]

(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; UC: FrameMove



;; UC: Meghanada

;g  (require 'meghanada)
                                        ;g  (add-hook 'java-mode-hook
                                        ;g            (lambda ()
                                        ;g              (meghanada-mode t)
                                        ;g              (gradle-mode t)
                                        ;g              (add-hook 'before-save-hook 'delete-trailing-whitespace)))
                                        ;g  (add-hook 'groovy-mode-hook
                                        ;g            (lambda ()
                                        ;g             (gradle-mode t)))
(message "drgeb loaded and configured: UC Meghanada")

;; UC: Zonokai Theme

;;   (zonokai-emacs :local (recipe
;;                          :fetcher github
;;                          :repo "https://github.com/ZehCnaS34/zonokai-emacs.git"))
;;   (load-theme 'zonokai-blue t)
;;   (add-to-list 'custom-theme-load-path "~/Programming/git/bitbucket.org/drgeb/dotspacemacs/site-lisp/zonokai-emacs")
;;   (load-theme 'zonokai-blue t)

;; UC: Monokai Theme

(add-to-list 'custom-theme-load-path "~/Programming/git/bitbucket.org/drgeb/dotspacemacs/site-lisp/monokai-emacs")
(load-theme 'monokai t)

;; UC: YASnippet

(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1)
  :config
;; — Found:
;;   https://emacsr.stackexchange.com/questions/9670/yasnippet-not-working-with-auto-complete-mode
;; ‐
(define-key yas-minor-mode-map (kbd "<tab>") nil)
(define-key yas-minor-mode-map (kbd "TAB") nil)
(define-key yas-minor-mode-map (kbd "<C-tab>") 'yas-expand)
;;g(yasnippet backquote-change)
;; - Found
;;   https://joaotavora.github.io/yasnippet/snippet-expansion.html
;;
)
(setq yas-prompt-functions '(yas-x-prompt yas-dropdown-prompt))

(use-package yasnippet-snippets
  :ensure t)

;; UC: Popup

(use-package popup
  :ensure t)
;; add some shotcuts in popup menu mode
(define-key popup-menu-keymap (kbd "M-n") 'popup-next)
(define-key popup-menu-keymap (kbd "TAB") 'popup-next)
(define-key popup-menu-keymap (kbd "<tab>") 'popup-next)
(define-key popup-menu-keymap (kbd "<backtab>") 'popup-previous)
(define-key popup-menu-keymap (kbd "M-p") 'popup-previous)
(defun yas/popup-isearch-prompt (prompt choices &optional display-fn)
  (when (featurep 'popup)
    (popup-menu*
     (mapcar
      (lambda (choice)
        (popup-make-item
         (or (and display-fn (funcall display-fn choice))
             choice)
         :value choice))
      choices)
     :prompt prompt
     ;; start isearch mode immediately
     :isearch t
     )))
(setq yas/prompt-functions '(yas/popup-isearch-prompt yas/no-prompt))
(message "drgeb loaded and configured: UC Popup")

;; UC: shell-pop

(use-package shell-pop
  :ensure t
  :bind (("s-t" . shell-pop))
  :config
  (setq shell-pop-shell-type (quote ("ehell" "eshell" (lambda nil (eshell)))))
  (setq shell-pop-term-shell "eshell")
  ;; need to do this manually or not picked up by `shell-pop'
  (shell-pop--set-shell-type 'shell-pop-shell-type shell-pop-shell-type))

;; UC: Incomming call

(global-set-key (kbd "<C-f2>")
                (lambda() (interactive)
                  (find-file "~/DropBox/MyLifeInOrg/DailyPlan.org")
                  (beginning-of-buffer)
                  (search-forward "2017-09-05 Tuesday")
                  (outline-show-children)
                  ;; (move-beginning-of-line)
                  ;; (next-line)
                  ;; (insert "phc")
                  ;; (yas-snippet-expand)
                  ))

;; UC: FASD Config

;;(use-package fasd
;;
;; :ensure t
;;             :init
;;             (global-fasd-mode t)
;;)

;; UC: Skewer-mode

;;GEb  (use-package  skewer-mode
;;GEB    :ensure t
;;GEB    :init
;;GEB    :config
;;GEB  )

;; UC: Flycheck
;;   ;; Flycheck Configation
;;   ;; http://www.flycheck.org/manual/latest/index.html

(use-package flycheck
    :straight t
    :config
    (global-flycheck-mode t)
    (add-hook 'sh-mode-hook 'flycheck-mode) ;;BASH Linting in Emacs
    (add-hook 'after-init-hook #'global-flycheck-mode) ;; turn on flychecking globally
    (flycheck-add-mode 'html-tidy 'web-mode)
    (flycheck-add-mode 'css-csslint 'web-mode)
;;    (flycheck-add-mode 'skewer-mode 'web-mode)
    (add-to-list 'flycheck-checkers 'd-ldc)
    ;; disable jshint since we prefer eslint checking
    (setq-default flycheck-disabled-checkers
                  (append flycheck-disabled-checkers
                          '(javascript-jshint)))
    ;; use eslint with web-mode for jsx files
    (flycheck-add-mode 'javascript-eslint 'web-mode)
    ;; customize flycheck temp file prefix
    (setq-default flycheck-temp-prefix ".flycheck")
    ;; disable json-jsonlist checking for json files
    (setq-default flycheck-disabled-checkers
                  (append flycheck-disabled-checkers
                          '(json-jsonlist)))
    )

;; UC: paredit

;;--------------------------------
;; Enable some handy paredit functions in all prog modes. REM to get it!
;;--------------------------------
                                        ;g(require 'paredit-everywhere)
                                        ;g(add-hook 'prog-mode-hook 'paredit-everywhere-mode)
                                        ;g(add-hook 'css-mode-hook 'paredit-everywhere-mode)
                                        ;g(add-hook 'emacs-lisp-mode-hook #'enable-paredit-mode)

;; UC: IBuffer
;;   See: [[http://cestlaz.github.io/posts/using-emacs-34-ibuffer-emmet/#.WbRrNkm0nY8][Using Emacs - 34 - ibuffer and emmet]]

(global-set-key (kbd "C-x C-b") 'ibuffer)
(setq ibuffer-saved-filter-groups
      (quote (("default"
               ("dired" (mode . dired-mode))
               ("org" (name . "^.*org$"))
               ("web" (or (mode . web-mode) (mode . js2-mode)))
               ("shell" (or (mode . eshell-mode) (mode . shell-mode)))
               ("mu4e" (name . "\*mu4e\*"))
               ("programming" (or
                               (mode . python-mode)
                               (mode . c++-mode)))
               ("emacs" (or
                         (name . "^\\*scratch\\*$")
                         (name . "^\\*Messages\\*$")))
               ))))
(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-auto-mode 1)
            (ibuffer-switch-to-saved-filter-groups "default")))
;; don't show these
                                        ;(add-to-list 'ibuffer-never-show-predicates "zowie")
;; Don't show filter groups if there are no buffers in that group
(setq ibuffer-show-empty-filter-groups nil)
;; Don't ask for confirmation to delete marked buffers
(setq ibuffer-expert t)

;; UC: Emmet mode

(use-package emmet-mode
  :ensure t
  :config
  (add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
  (add-hook 'web-mode-hook  'emmet-mode) ;; Auto-start on any markup modes
  (add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.
  )

;; UC: flow + auto-complete.el

(use-package auto-complete
  :ensure t
  :init
  (progn
    (ac-config-default)
    (global-auto-complete-mode t))
  :config
  )

;; UC: flow

(use-package json
                :ensure t)
(defvar flow-program "flow")
(defun flow--get-completions ()
  (assoc-default
   'result
   (json-read-from-string
    (with-output-to-string
      (let ((line (line-number-at-pos))
            (col (1+ (current-column))))
        (call-process-region
         (point-min) (point-max) flow-program nil standard-output nil
         "autocomplete"
         "--json"
         (number-to-string line)
         (number-to-string col)))))))
(defun flow--ac-prefix ()
  (when (re-search-backward "\\(?:\\.\\)\\(\\(?:[a-zA-Z0-9][_a-zA-Z0-9]*\\)?\\)\\=" nil t)
    (match-beginning 1)))
(defun flow--ac-candidates ()
  (mapcar
   (lambda (c)
     (popup-make-item
      (assoc-default 'name c)
      :summary (assoc-default 'type c)))
   (flow--get-completions)))
(ac-define-source flow-program
  '((candidates . flow--ac-candidates)
    (prefix . flow--ac-prefix)
    (requires . 0)))
;; Load the default configuration
;; auto-complete configuration
;; Make sure we can find the dictionaries
(add-to-list 'ac-dictionary-directories "~/.emacs.d/elpa/auto-complete-20170124.1845/dict")
(add-to-list 'ac-dictionary-directories "~/.emacs.d/elpa/ansible-20161218.1707/dict")
;; Use dictionaries by default
;;G (setq-default ac-sources (add-to-list 'ac-sources 'ac-source-dictionary))
(setq global-auto-complete-mode t)
;; Start auto-completion after 2 characters of a word
(setq ac-auto-start 2)
;; case sensitivity is important when finding matches
(setq ac-ignore-case nil)

;; UC: Banner

(add-to-list 'warning-suppress-types '(yasnippet backquote-change))

;; UC: Line Comment Banner

(eval-and-compile
    (defun comment-banner-load-path ()
      (shell-command "find ~ -path ~/Programming/git/bitbucket.org/drgeb/repos/emacs/packages/line-comment-banner/")))
;;  (use-package line-comment-banner
;;    :ensure t
;;  ;;;      :load-path (lambda () (list (comment-banner-load-path)))
;;    :load-path "~/repos/emacs/packages/line-comment-banner"
;;        :config
;;      ;;;;  (global-set-key (kbd "C-;") (lambda () (interactive) 'line-comment-banner))
;;        (global-set-key (kbd "C-;") 'line-comment-banner)
;;       )

;; UC: Hide

(add-hook 'c-mode-common-hook
          (lambda()
            (local-set-key (kbd "C-c <right>") 'hs-show-block)
            (local-set-key (kbd "C-c <left>")  'hs-hide-block)
            (local-set-key (kbd "C-c <up>")    'hs-hide-all)
            (local-set-key (kbd "C-c <down>")  'hs-show-all)
            (hs-minor-mode t)))

;; UC: Ivy
;;   :Date:Monday, April 16, 2018 :END:
;;   See: [[https://sam217pa.github.io/2016/09/13/from-helm-to-ivy/][From heml to Ivy]]

(use-package ivy :ensure t
  :diminish (ivy-mode . "")
  :bind
  (:map ivy-mode-map
        ("C-'" . ivy-avy))
  :config
  (ivy-mode 1)
  ;; add ‘recentf-mode’ and bookmarks to ‘ivy-switch-buffer’.
  (setq ivy-use-virtual-buffers t)
  ;; number of result lines to display
  (setq ivy-height 10)
  ;; does not count candidates
  (setq ivy-count-format "")
  ;; no regexp by default
  (setq ivy-initial-inputs-alist nil)
  ;; configure regexp engine.
  (setq ivy-re-builders-alist
        ;; allow input not in order
        '((t   . ivy--regex-ignore-order))))

;; UC: Ivy-mode COMMENT

(ivy-mode 1)
  ;; See: https://www.youtube.com/watch?v=AaUlOH4GTCs
  (ivy-set-actions
   'councel-find-file
   '(("j" find-file-other-frame "other frame")
     ("b" counsel-find-file-cd-bookmark-action "cd bookmark")
     ("x" counsel-find-file-extern "open externally")
     ("r" counsel-find-file-as-root "open as root")
     ("d" counsel-find-file-mkdir-action "mkdir")))
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; End Ivy ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (use-package swiper
    :ensure t
    :config
     (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t)
  (global-set-key (kbd "C-H-s") 'swiper)
  (global-set-key (kbd "C-s") 'isearch-forward)
  (global-set-key (kbd "C-c C-r") 'ivy-resume)
  (global-set-key (kbd "<f6>") 'ivy-resume)
  (global-set-key (kbd "M-x") 'counsel-M-x)
  (global-set-key (kbd "C-x C-f") 'counsel-find-file)
  (global-set-key (kbd "<f1> f") 'counsel-describe-function)
  (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
  (global-set-key (kbd "<f1> l") 'counsel-find-library)
  (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
  (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
  (global-set-key (kbd "C-c g") 'counsel-git)
  (global-set-key (kbd "C-c j") 'counsel-git-grep)
  (global-set-key (kbd "C-c k") 'counsel-ag)
  (global-set-key (kbd "C-x l") 'counsel-locate)
  (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
  (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
)

;; UC: Counsel

(all-the-icons-ivy-setup)
(use-package ivy :demand
  :config
  (setq ivy-use-virtual-buffers t
        ivy-count-format "%d/%d "))
;;set action options during execution of counsel-find-file
;; replace "frame" with window to open in new window
(ivy-set-actions
 'counsel-find-file
 '(("j" find-file-other-frame "other frame")
   ("b" counsel-find-file-cd-bookmark-action "cd bookmark")
   ("x" counsel-find-file-extern "open externally")
   ("d" delete-file "delete")
   ("r" counsel-find-file-as-root "open as root")))
;; set actions when running C-x b
;; replace "frame" with window to open in new window
(ivy-set-actions
 'ivy-switch-buffer
 '(("j" switch-to-buffer-other-frame "other frame")
   ("k" kill-buffer "kill")
   ("r" ivy--rename-buffer-action "rename")))
;;
;;
;; End Ivy, Swiper, Counsel

;; UC: mmm-mode

(use-package mmm-mode
  :ensure t
  :config
(setq mmm-global-mode 'maybe)
(mmm-add-mode-ext-class 'html-mode "\\.php\\'" 'html-php)
(mmm-add-mode-ext-class 'html-mode nil 'mason)
  (mmm-add-mode-ext-class nil "\\.nw\\'" 'noweb))

;; UC: INSERT ORG MODELINE Function C-c i

(defconst *org-modeline*  "# -*- mode:org; -*-")
  (defconst *org-mode-settings* "
#+TITLE:
#+AUTHOR: Dr. Gerald E. Bennett
#+DESCRIPTION:
#+LANGUAGE: en
  #+STARTUP: showall
  #+STARTUP: hidestars
  #+OPTIONS: toc:nil
  #+OPTIONS: skip:t
  #+HTML_HEAD: <link rel=\"stylesheet\" type=\"text/css\" href=\"./org.css\" />
  #+OPTIONS: ^:nil
  \* Heading
  ")
  (defun insert-org-modeline ()
    (interactive)
    (save-excursion
      (beginning-of-buffer)
      (insert *org-modeline*)
      (insert *org-mode-settings*)
      (newline)
      (newline)
      )
    (end-of-buffer)
    (save-buffer))
  (global-set-key (kbd "C-c i") 'insert-org-modeline)

;; UC: sgml -  evil-mode and xml folding

(use-package sgml-mode
  :ensure t
  :config
(add-to-list 'hs-special-modes-alist
             '(nxml-mode
               "<!--\\|<[^/>]*[^/]>"
               "-->\\|</[^/>]*[^/]>"
               "<!--"
               sgml-skip-tag-forward
               nil))
  (add-hook 'nxml-mode-hook 'hs-minor-mode))

;; UC: Org-todo Configurations

;;       (setq org-todo-keywords
;;             '(((sequence "☛ TODO(t)" "STARTED" "✔ DONE(d)" "✘ CANCELED(c)" "|" "⚑ WAITING(w)" "✔ DONE(d)"))))
;;       (setq org-todo-keyword-faces
;;             '(("☛ TODO(t)" . org-warning)
;;               ("STARTED" . "yellow")
;;               ("✘ CANCELED(c)" . (:foreground "blue" :weight bold))))

;; UC: Adaptive wrap
;;   See: [[https://emacs.stackexchange.com/questions/14589/correct-indentation-for-wrapped-lines][Correct indentation for wrapped lines]]

;; this is from my .emacs.d
    ;; (require 'adaptive-wrap)
    ;;   (with-eval-after-load 'adaptive-wrap
    ;;     (setq-default adaptive-wrap-extra-indent 4))
    ;;   (add-hook 'visual-line-mode-hook
    ;;             (lambda ()
    ;;               (adaptive-wrap-prefix-mode +1)
    ;;              (diminish 'visual-line-mode)))
    ;;
  (when (fboundp 'adaptive-wrap-prefix-mode)
    (defun my-activate-adaptive-wrap-prefix-mode ()
      "Toggle `visual-line-mode' and `adaptive-wrap-prefix-mode' simultaneously."
      (adaptive-wrap-prefix-mode (if visual-line-mode 1 -1)))
    (add-hook 'visual-line-mode-hook 'my-activate-adaptive-wrap-prefix-mode)
    (global-visual-line-mode +1)
)

;; UC: Skewer mode

;  (use-package skewer-mode
;    :defer t
;    :config
;    ;(skewer-setup)
;    )

;; UC: JSX

;; use web-mode for .jsx files
  ;; (add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))
  ;; https://github.com/purcell/exec-path-from-shell
  ;; only need exec-path-from-shell on OSX
  ;; this hopefully sets up path and other vars better
;;  (when (memq window-system '(mac ns))
;;    (exec-path-from-shell-initialize))
  ;; use local eslint from node_modules before global
  ;; http://emacs.stackexchange.com/questions/21205/flycheck-with-file-relative-eslint-executable
  (defun my/use-eslint-from-node-modules ()
    (let* ((root (locate-dominating-file
                  (or (buffer-file-name) default-directory)
                  "node_modules"))
           (eslint (and root
                        (expand-file-name "node_modules/eslint/bin/eslint.js"
                                          root))))
      (when (and eslint (file-executable-p eslint))
        (setq-local flycheck-javascript-eslint-executable eslint))))
  (add-hook 'flycheck-mode-hook #'my/use-eslint-from-node-modules)
  ;; adjust indents for web-mode to 2 spaces
  (defun my-web-mode-hook ()
    "Hooks for Web mode. Adjust indents."
    ;;; http://web-mode.org/
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-css-indent-offset 2)
    (setq web-mode-code-indent-offset 2))
  (add-hook 'web-mode-hook  'my-web-mode-hook)
  ;; for better jsx syntax-highlighting in web-mode
  ;; - courtesy of Patrick @halbtuerke
  (defadvice web-mode-highlight-part (around tweak-jsx activate)
    (if (equal web-mode-content-type "jsx")
        (let ((web-mode-enable-part-face nil))
          ad-do-it)
      ad-do-it))

;; Issue
;;   See: https://github.com/syl20bnr/spacemacs/issues/6765
;;   See: [[https://github.com/syl20bnr/spacemacs/issues/5435#issuecomment-195862080][Auto paste into the files opened by mouse-click the item below Recent Files in Spacemacs buffer]]

;;     (define-key spacemacs-buffer-mode-map [down-mouse-1] nil)
(add-hook 'spacemacs-buffer-mode-hook (lambda ()
                                        (set (make-local-variable 'mouse-1-click-follows-link) nil)))

;; UC: Prodigy

(use-package prodigy
  :ensure t
  :config
  (global-set-key (kbd "C-x M-m") 'prodigy))

;; UC: Smex (disabled)
;;   ;; Smart M-x is smart(require 'smex)
;;   ;; See: [[https://www.emacswiki.org/emacs/Smex][Smex Emacs Wiki]]

;; Automatically done (smex-initialize)

;; UC: Meghanana

(setq meghanada-auto-start nil)

;; UC: Menu Bar
;;     If you need the menu bar, you are lost, anyway.

(menu-bar-mode -1)
(global-set-key [f2] 'toggle-menu-bar-mode-from-frame)

;; UC: My Own Expansions
;;     See: [[http://www.nicholasvanhorn.com/posts/org-structure-completion.html][Org-mode template expansions for easy code block insertion]]
;;     See: [[https://blog.aaronbieber.com/2016/11/23/creating-org-mode-structure-templates.html][Creating Org Mode Structure Templates]]

(eval-after-load 'org
  '(progn
     (add-to-list 'org-structure-template-alist '("el" . "elisp"))))
(eval-after-load 'org
  '(progn
     (add-to-list 'org-structure-template-alist '("go" . "go"))))
(eval-after-load 'org
  '(progn
     (add-to-list 'org-structure-template-alist '("j" . "java"))))
(eval-after-load 'org
  '(progn
     (add-to-list 'org-structure-template-alist '("py" . "python"))))
(eval-after-load 'org
  '(progn
     (add-to-list 'org-structure-template-alist '("sh" . "shell"))))
(eval-after-load 'org
  '(progn
     (add-to-list 'org-structure-template-alist '("sql" . "sql"))))

;; UC: All Icons Dired

(setq inhibit-compacting-font-caches t)
    (use-package all-the-icons-dired
      :ensure t
      :init
      :config
    (add-hook 'dired-mode-hook 'all-the-icons-dired-mode))

;; UC: Aggresive Indent
;;   SEE:  [[https://github.com/Malabarba/aggressive-indent-mode][Aggresive Indent]]

(use-package aggressive-indent
  :ensure t
  :init
  :config
  (global-aggressive-indent-mode 1)
  (add-to-list 'aggressive-indent-excluded-modes 'html-mode))
;;    (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
;;    (add-hook 'css-mode-hook #'aggressive-indent-mode))

;; UC: Insert Date

(message "drgeb UC: Insert Date")
(defun insdate-insert-current-date (&optional omit-day-of-week-p)
  "Insert today's date using the current locale.
   With a prefix argument, the date is inserted without the day of
   the week."
  (interactive "P*")
  (insert (calendar-date-string (calendar-current-date) nil
                                omit-day-of-week-p)))
(global-set-key "\C-x\M-d" `insdate-insert-current-date)

;; UC: [[https://github.com/syl20bnr/spacemacs/issues/1269][Issue]]

(defun enable-solarized-in-terminal (frame)
  ;; cf. http://philipdaniels.com/blog/2017/02/spacemacs---configuring-the-solarized-theme/
  (unless (display-graphic-p frame)
    (set-frame-parameter frame 'background-mode 'dark)
    (set-terminal-parameter frame 'background-mode 'dark)
    (spacemacs/load-theme 'solarized)
    ))
  (defun enable-solarized-in-gui ()
    (mapc 'disable-theme custom-enabled-themes)
    (setup-solarized-theme)
    (spacemacs/load-theme 'solarized)
    )
(defun setup-solarized-theme ()
  "Setup Solarized theme."
  "I like a transparent background in the terminal."
  (custom-set-faces
   '(default (
              (((type tty) (background dark)) (:background "nil"))
              )))
  ;; The frame number indicator has some problems with inverse video
  (set-face->inverse-video 'spacemacs
                           -motion-face nil)
  (set-face-inverse-video 'spacemacs-insert-face nil)
  (set-face-inverse-video 'spacemacs-normal-face nil)
  (set-face-inverse-video 'spacemacs-visual-face nil)
  (set-face-inverse-video 'spacemacs-replace-face nil))
;; For GUI clients, after-make-frame-functions is messed up (probably it is
;; called too early), and some colors are messed up. For terminal clients on
;; the other hand, it works perfectly. The background-mode is necessary for
;; terminal clients, but (apparently) not for GUI clients, and has to be set
;; for each frame individually.
;; (spacemacs|do-after-display-system-init (enable-solarized-in-gui))
(add-hook 'after-make-frame-functions 'enable-solarized-in-terminal)

;; UC: ------------------------------                           :noexport:

'(org-directory "~/Dropbox/MyLifeInOrg/gtd")
'(org-export-html-postamble nil)

;; UC: Leading Stars in Headlines
;;    Hide the leading stars in Headlines (this works not good enough)

'(org-hide-leading-stars t)

;; UC: ?

'(org-startup-folded t)
'(org-startup-indented t)

;; UC: Line Number Mode Settings

;;GEB  (setq linum-format "%4d \u2502 ")
(setq-default left-fringe-width  10)
(setq-default right-fringe-width  0)
(set-face-attribute 'fringe nil :background "black")

;; UC: Markdown-mode

;; https://jblevins.org/projects/markdown-mode/
  (use-package markdown-mode
    :ensure t
    :commands (markdown-mode gfm-mode)
    :mode (("README\\.md\\'" . gfm-mode)
           ("\\.md\\'" . markdown-mode)
           ("\\.markdown\\'" . markdown-mode))
    :init (setq markdown-command "multimarkdown"))
(when (configuration-layer/layer-usedp 'markdown)
  (setq auto-mode-alist (cons '("\\.text$" . gfm-mode) auto-mode-alist))
  (setq auto-mode-alist (cons '("\\.md$" . gfm-mode) auto-mode-alist))
  (setq auto-mode-alist (cons '("\\.mdown$" . gfm-mode) auto-mode-alist))
  (setq auto-mode-alist (cons '("\\.mdt$" . gfm-mode) auto-mode-alist))
  (setq auto-mode-alist (cons '("\\.markdown$" . gfm-mode) auto-mode-alist)))

;; UC: Python

(setq py-python-command "python3")
(setq python-shell-interpreter "python3")
(use-package elpy
  :ensure t
  :config
  (elpy-enable))
(use-package virtualenvwrapper
  :ensure t
  :config
  (venv-initialize-interactive-shells)
  (venv-initialize-eshell))

;; UC: Javascript

(use-package js2-mode
  :ensure t
  :ensure ac-js2
  :init
  (progn
    (add-hook 'js-mode-hook 'js2-minor-mode)
    (add-hook 'js2-mode-hook 'ac-js2-mode)
    ))
(use-package js2-refactor
  :ensure t
  :config
  (progn
    (js2r-add-keybindings-with-prefix "C-c C-m")
    ;; eg. extract function with `C-c C-m ef`.
    (add-hook 'js2-mode-hook #'js2-refactor-mode)))
(use-package tern
  :ensure tern
  :ensure tern-auto-complete
  :config
  (progn
    (add-hook 'js-mode-hook (lambda () (tern-mode t)))
    (add-hook 'js2-mode-hook (lambda () (tern-mode t)))
    (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
    ;;(tern-ac-setup)
    ))

;;(use-package jade
;;:ensure t
;;)

;; use web-mode for .jsx files
(add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))

;; http://www.flycheck.org/en/latest/user/quickstart.html#flycheck-quickstart
;; turn on flychecking globally
(add-hook 'after-init-hook #'global-flycheck-mode)

;; disable jshint since we prefer eslint checking
(setq-default flycheck-disabled-checkers
              (append flycheck-disabled-checkers
                      '(javascript-jshint)))

;; use eslint with web-mode for jsx files
(flycheck-add-mode 'javascript-eslint 'web-mode)

;; customize flycheck temp file prefix
(setq-default flycheck-temp-prefix ".flycheck")

;; disable json-jsonlist checking for json files
(setq-default flycheck-disabled-checkers
              (append flycheck-disabled-checkers
                      '(json-jsonlist)))

;; UC: React JS2-mode configuration

(setq-default
 ;; js2-mode
 js2-basic-offset 2
 ;; web-mode
 css-indent-offset 2
 web-mode-markup-indent-offset 2
 web-mode-css-indent-offset 2
 web-mode-code-indent-offset 2
 ;; web-mode-attr-indent-offset 2
 )
(with-eval-after-load 'web-mode
  (add-to-list 'web-mode-indentation-params '("lineup-args" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-concats" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-calls" . nil)))

;; UC: Autofill mode

(add-hook 'text-mode-hook 'auto-fill-mode)
(setq-default fill-column 80)

;; UC: Edit Files as Root
;;     See: [[http://emacsredux.com/blog/2013/04/21/edit-files-as-root/][Emacs Redux]]

(global-set-key (kbd "C-x C-r") 'sudo-edit)

;; UC: Tramp

(use-package tramp
  :ensure nil
  :custom
  (tramp-verbose 9)
  (tramp-default-method "ssh")
  (tramp-ssh-controlmaster-options
   (concat "-o ControlPath=/tmp/tramp.%%r@%%h:%%p "
           "-o ControlMaster=auto "
           "-o ControlPersist=no"))
   :config
     (when (eq window-system 'w32)
       (setq tramp-default-method "plink")
       (when (and (not (string-match putty-directory (getenv "PATH")))
                        (file-directory-p putty-directory))
         (setenv "PATH" (concat putty-directory ";" (getenv "PATH")))
         (add-to-list 'exec-path putty-directory))))

;; UC: insert-shebang

(use-package insert-shebang
  :ensure t
  :init
  :config
  (add-hook 'find-file-hook 'insert-shebang)
  (setq insert-shebang-ignore-extensions '("txt" "org" "go" "java" "yaml" "json" "html" "js" "css"))
  )

;; UC: F2 to open my init file

(defun open-my-init-file()
  "This will just open my initialization file."
  (interactive)
  (find-file (concat dotspacemacs-directory "drgeb.org")))
(global-set-key (kbd "<f2>") 'open-my-init-file)
(spacemacs/set-leader-keys "oo" 'open-my-init-file)

;; Wgrep
;;      See Zamansky [[https://www.youtube.com/watch?v=-sFTkCQ774o&t=6s][Using Emacs 48 - silversearcher]]

(use-package wgrep
:ensure t
)
(use-package wgrep-ag
:ensure t
)

;; UC: Dumb jump

(use-package dumb-jump
  :bind (("M-g o" . dumb-jump-go-other-window)
         ("M-g j" . dumb-jump-go)
         ("M-g x" . dumb-jump-go-prefer-external)
         ("M-g z" . dumb-jump-go-prefer-external-other-window))
  :config
  ;; (setq dumb-jump-selector 'ivy) ;; (setq dumb-jump-selector 'helm)
:init
(dumb-jump-mode)
  :ensure
)

;; UC: Silversearcher
;;   See Zamansky [[https://www.youtube.com/watch?v=-sFTkCQ774o&t=6s][Using Emacs 48 - silversearcher]]

(use-package ag
  :ensure t)

;; UC: MAC SuperKey

(setq ns-right-option-modifer 'super)
;; (setq mac-command-modifier 'meta) ; make cmd key do Meta
;; (setq mac-option-modifier 'super) ; make opt key do Super
;; (setq mac-control-modifier 'control) ; make Control key do Control
;; (setq ns-function-modifier 'hyper)  ; make Fn key do Hyper
;;  (setq mac-option-key-is-meta nil
;;        mac-command-key-is-meta t

;; UC: Company-mode

;; (use-package company-mode
;; :ensure t
;; )

;; UC: Company-Go

(use-package company-go
:ensure t
)

;; UC: Go-AutoComplete

(use-package go-mode
  :ensure t
  )
(with-eval-after-load 'go-mode
  (load go-autocomplete-file)
  (require 'go-autocomplete)
  (message "go-autocomplete loaded")
  (defun auto-complete-for-go ()
    (auto-complete-mode 1))
  (add-hook 'go-mode-hook 'auto-complete-for-go))

;; UC: GoREPL

(use-package gorepl-mode
  :ensure t
  :init
  :config
  (add-hook 'go-mode-hook #'gorepl-mode)
  )

;; US: GoFlyMake

(use-package flymake-go :ensure t)

;; UC: GoFlyCheck

(use-package flycheck-gometalinter
  :ensure t
)

;; UC: GOElDoc

(use-package go-eldoc
  :ensure t )

;; UC: GODef && Automatically format GO Codeon save

(add-to-list 'exec-path "~/Programming/go-workspace/bin")
(defun my-go-mode-hook ()
  "Use goimports instead of go-fmt, for mygo-mode."
  (add-hook 'before-save-hook 'gofmt-before-save)
  ; Godef jump key binding
  (local-set-key (kbd "M-.") 'godef-jump)
  (local-set-key (kbd "M-*") 'pop-tag-mark)
  )
(add-hook 'go-mode-hook 'my-go-mode-hook)

;; UC: Autocomplete for Go

(defun auto-complete-for-go ()
  "I have no clue what auto-complete-for-go function does."
  (auto-complete-mode 1))
(add-hook 'go-mode-hook 'auto-complete-for-go)
(with-eval-after-load 'go-mode
  (use-package go-autocomplete :ensure t))

;; UC: Compile go
;;      See: [[Configure Emacs as a Go Editor]]
;;      [[https://stackoverflow.com/questions/42313358/go-oracle-not-found][Go Oracle not found?
;; ]]     See: [[https://tleyden.github.io/blog/2014/05/27/configure-emacs-as-a-go-editor-from-scratch-part-2/][Configure Emacs as a Go Editor From Scratch Part 2]]

(defun my-go-mode-hook ()
    "Another hook mode function!"
    ;; Use goimports instead of go-fmt
    (setq gofmt-command "goimports")
    ;; Call Gofmt before saving
    (add-hook 'before-save-hook 'gofmt-before-save)
    ;; Customize compile command to run go build
    (if (not (string-match "go" compile-command))
        (set (make-local-variable 'compile-command)
             "go generate && go build -v && go test -v && go vet"))
    ;; Go oracle
;;GEB    (load-file "$GOPATH/src/golang.org/x/tools/cmd/oracle/oracle.el")
    ;; Godef jump key binding
    (local-set-key (kbd "M-.") 'godef-jump)
    (local-set-key (kbd "M-*") 'pop-tag-mark)
    ;; eldoc
    (add-hook 'go-mode-hook 'go-eldoc-setup)
    )
    (add-hook 'go-mode-hook 'my-go-mode-hook)

;; UC: Atomic Chrome (edit in emacs)

(use-package atomic-chrome
:ensure t
:config (atomic-chrome-start-server))
(setq atomic-chrome-buffer-open-style 'frame)

;; UC: Regex

(use-package pcre2el
  :ensure t
  :config
  (pcre-mode)
  )

;; UC: Music

(use-package simple-mpc
  :ensure t)
(use-package mingus
  :ensure t)

;; UC: auto-yasnippet

(use-package auto-yasnippet
       :ensure t)
;;       (setq yas-snippet-dirs (append yas-snippet-dirs
;;                               '("~/Downloads/interesting-snippets")))

;; UC: aws-ec2 aws-snippets

(use-package aws-ec2
  :ensure t)
(use-package aws-snippets
  :ensure t)

;; UC: PDF-Tools-install

;;(use-package pdf-tools
;; :ensure t
;; :config
;; (custom-set-variables
;;   '(pdf-tools-handle-upgrades nil)) ; Use brew upgrade pdf-tools instead.
;; (setq pdf-info-epdfinfo-program "/usr/local/bin/epdfinfo"))
;; (pdf-tools-install)

;; UC: ------------------------------                           :noexport:

(req-package-finish)

;; UC: org-mode
;;     Unicode-characters fuer bullets in orgmode

(use-package org-bullets
:ensure t
:init
:config
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;; UC: ------------------------------                           :noexport:

(req-package-finish)

;; UC: kubernetes

(use-package kubernetes
  :ensure t
  :commands (kubernetes-overview))

;; If you want to pull in the Evil compatibility package.
(use-package kubernetes-evil
  :ensure t
  :after kubernetes)

;; UC: feature-mode for Cucumber BDD Testing

;;      (use-package feaure-mode
;;              :ensure t)
;;      (add-to-list 'auto-mode-alist '("\.feature$" . feature-mode))

;; UC: confluence



;; UC: org-roam

(use-package org-roam
      :ensure t
      :hook
      (after-init . org-roam-mode)
      :custom
      (make-directory "/Users/drgeb/Programming/git/bitbucket.org/drgeb/daily/org-roam")
      (org-roam-directory "/Users/drgeb/Programming/git/bitbucket.org/drgeb/daily/org-roam")
      :bind (:map org-roam-mode-map
              (("C-c n l" . org-roam)
              ("C-c n f" . org-roam-find-file)
              ("C-c n g" . org-roam-graph-show))
              :map org-mode-map
              (("C-c n i" . org-roam-insert))
              (("C-c n I" . org-roam-insert-immediate))))

;; UC: org-roam protocol extentions

(use-package org-roam-protocol
  :ensure t)

;; UC: org-roam packages

(defconst org-roam-packages
  '(org-roam org-roam-bibtex))

(defun org-roam/init-org-roam-bibtex ()
  (use-package org-roam-bibtex
    :after org-roam
    :hook (org-roam-mode . org-roam-bibtex-mode)
    :bind (:map org-mode-map
                (("C-c n a" . orb-note-actions)) )))

;; UC: org-roam server

(use-package org-roam-server
  :ensure t
  :config
  (setq org-roam-server-host "127.0.0.1"
        org-roam-server-port 8080
        org-roam-server-export-inline-images t
        org-roam-server-authenticate nil
        org-roam-server-network-poll t
        org-roam-server-network-arrows nil
        org-roam-server-network-label-truncate t
        org-roam-server-network-label-truncate-length 60
        org-roam-server-network-label-wrap-length 20))

;; UC: org-journal

(use-package org-journal
  :bind (("C-c t" . journal-file-today)
         ("C-c y" . journal-file-yesterday)
         ("C-c r j" . org-journal-new-entry))
  :custom
  (org-journal-dir org-journal-directory)
  (org-journal-enable-agenda-integration t)
  (org-journal-file-format "%Y%m%d")
  (org-journal-date-format "%e %b %Y (%A)")
  (org-journal-time-format "")
  :preface
  (defun get-journal-file-today ()
    "Gets filename for today's journal entry."
    (let ((daily-name (format-time-string "%Y%m%d")))
      (expand-file-name (concat org-journal-dir daily-name))))

  (defun journal-file-today ()
    "Creates and load a journal file based on today's date."
    (interactive)
    (find-file (get-journal-file-today)))

  (defun get-journal-file-yesterday ()
    "Gets filename for yesterday's journal entry."
    (let* ((yesterday (time-subtract (current-time) (days-to-time 1)))
           (daily-name (format-time-string "%Y%m%d" yesterday)))
      (expand-file-name (concat org-journal-dir daily-name))))

  (defun journal-file-yesterday ()
    "Creates and load a file based on yesterday's date."
    (interactive)
    (find-file (get-journal-file-yesterday)))
)

;; UC: org-download

(use-package org-download
  :ensure t
  :after org
  :bind
  (:map org-mode-map
        (("s-Y" . org-download-screenshot)
         ("s-y" . org-download-yank)))
  :config
  (setq-default org-download-image-dir org-download-image-directory)
  ;; Drag-and-drop to `dired`
  (add-hook 'dired-mode-hook 'org-download-enable))

;; UC: org-cliplink

(use-package org-cliplink
  :ensure t
  :bind
  ("C-x p i" . org-cliplink)
  )
(global-set-key (kbd "C-x p i") 'org-cliplink)
(defun custom-org-cliplink ()
  (interactive)
  (org-cliplink-insert-transformed-title
   (org-cliplink-clipboard-content)     ;take the URL from the CLIPBOARD
   (lambda (url title)
     (let* ((parsed-url (url-generic-parse-url url)) ;parse the url
            (clean-title
             (cond
              ;; if the host is github.com, cleanup the title
              ((string= (url-host parsed-url) "github.com")
               (replace-regexp-in-string "GitHub - .*: \\(.*\\)" "\\1" title))
              ;; otherwise keep the original title
              (t title))))
       ;; forward the title to the default org-cliplink transformer
       (org-cliplink-org-mode-link-transformer url clean-title)))))

;; UC: org-trello

(use-package org-trello
  :ensure t
  :init
  :config
  (message "drgeb loaded and configured: org-trello")
  ;;    (setq (org-trello-files (directory-files
  ;;                              "~/Dropbox/MyLifeInOrg/trello/Trello.org"
  ;;                             "~/Dropbox/MyLifeInOrg/trello/My_Emacs_Trello.org")))
  (add-hook 'org-trello-mode-hook
              (lambda ()
                (define-key org-trello-mode-map (kbd "C-c o v") 'org-trello-version)
                (define-key org-trello-mode-map (kbd "C-c o i") 'org-trello-install-key-and-token)
                (define-key org-trello-mode-map (kbd "C-c o I") 'org-trello-install-board-metadata)
                (define-key org-trello-mode-map (kbd "C-c o c") 'org-trello-sync-card)
                (define-key org-trello-mode-map (kbd "C-c o s") 'org-trello-sync-buffer)
                (define-key org-trello-mode-map (kbd "C-c o a") 'org-trello-assign-me)
                (define-key org-trello-mode-map (kbd "C-c o d") 'org-trello-check-setup)
                (define-key org-trello-mode-map (kbd "C-c o D") 'org-trello-delete-setup)
                (define-key org-trello-mode-map (kbd "C-c o b") 'org-trello-create-board-and-install-metadata)
                (define-key org-trello-mode-map (kbd "C-c o k") 'org-trello-kill-entity)
                (define-key org-trello-mode-map (kbd "C-c o K") 'org-trello-kill-cards)
                (define-key org-trello-mode-map (kbd "C-c o a") 'org-trello-archive-card)
                (define-key org-trello-mode-map (kbd "C-c o A") 'org-trello-archive-cards)
                (define-key org-trello-mode-map (kbd "C-c o j") 'org-trello-jump-to-trello-card)
                (define-key org-trello-mode-map (kbd "C-c o J") 'org-trello-jump-to-trello-board)
                (define-key org-trello-mode-map (kbd "C-c o C") 'org-trello-add-card-comments)
                (define-key org-trello-mode-map (kbd "C-c o o") 'org-trello-show-card-comments)
                (define-key org-trello-mode-map (kbd "C-c o l") 'org-trello-show-card-labels)
                (define-key org-trello-mode-map (kbd "C-c o u") 'org-trello-update-board-metadata)
                (define-key org-trello-mode-map (kbd "C-c o h") 'org-trello-help-describing-bindings)))
  )

;; UC: Graphviz dot mode
;; For editing Graphs.

(use-package graphviz-dot-mode)

;; UC: ox-clip

(use-package ox-clip
  :bind
  ("A-C-M-k" . ox-clip-formatted-copy))

;; UC: ox-md

(use-package ox-md
  :ensure nil
  :defer 3
  :after org)

;; UC: json key-type

(use-package json-key-type
  :ensure nil
  )
(use-package json-navigator
  :ensure nil
  )

;; UC: Anzu

(use-package anzu
    :ensure t
    )
  (global-anzu-mode +1)
  (anzu-mode +1)
  (global-set-key [remap query-replace] 'anzu-query-replace)
  (global-set-key [remap query-replace-regexp] 'anzu-query-replace-regexp)

  (defun my/anzu-update-func (here total)
    (when anzu--state
      (let ((status (cl-case anzu--state
                      (search (format "<%d/%d>" here total))
                      (replace-query (format "(%d Replaces)" total))
                      (replace (format "<%d/%d>" here total)))))
        (propertize status 'face 'anzu-mode-line))))

(custom-set-variables
 '(anzu-mode-line-update-function #'my/anzu-update-func))

(setq anzu-cons-mode-line-p nil)
(setcar (cdr (assq 'isearch-mode minor-mode-alist))
        '(:eval (anzu--update-mode-line)))

(executable-find "sqlite3")
