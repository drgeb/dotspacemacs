(setq max-lisp-eval-depth 10000)
(setq max-specpdl-size 10000)
(setq package-check-signature nil)

(require 'package)
(defvar gnu '("gnu" . "https://elpa.gnu.org/packages/"))
(defvar elpa '("elpa" . "https://tromey.com/elpa/"))
(defvar melpa '("melpa" . "https://melpa.org/packages/"))
(defvar melpa-stable '("melpa-stable" . "https://stable.melpa.org/packages/"))
(defvar marmalade '("marmalade" . "https://marmalade-repo.org/packages/"))
(defvar org-elpa '("org" . "http://orgmode.org/elpa/"))
;; Add marmalade to package repos
(setq package-archives nil)
(add-to-list 'package-archives melpa-stable t)
(add-to-list 'package-archives elpa t)
(add-to-list 'package-archives melpa t)
(add-to-list 'package-archives gnu t)
(add-to-list 'package-archives marmalade t)
(add-to-list 'package-archives org-elpa t)
(package-initialize)

;; Bootstrap straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)

;; Use-package as the package configuration tool of choice
(straight-use-package 'use-package)
;; Force use-package to use straight.el to automatically install missing packages
(setq straight-use-package-by-default t)

(require 'use-package)
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(defun packages-install (&rest packages)
  (message "running packages-install")
  (mapc (lambda (package)
          (let ((name (car package))
                (repo (cdr package)))
            (when (not (package-installed-p name))
              (let ((package-archives (list repo)))
                (package-initialize)
                (package-install name)))))
        packages))

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Define sandbox directory variable
(let* ((env (getenv "SANDBOX_HOME"))
       (env-dir (when env (expand-file-name (concat env "/"))))
       (env-init (and env-dir (expand-file-name "init.el" env-dir)))
       (no-env-dir-default (expand-file-name
                            (concat user-home-directory
                                    "sandbox/")))
       (default-init (expand-file-name ".spacemacs" user-home-directory)))

  (defconst sandbox-directory
    (cond
     ((and env (file-exists-p env-dir))
      env-dir)
     ((file-exists-p no-env-dir-default)
      no-env-dir-default)
     (t
      nil))
    "Optional sandbox directory, which defaults to
    ~/sandbox. This setting can be overridden using the
    SPACEMACSDIR environment variable. If neither of these
    directories exist, this variable will be nil."))

;; Define tools-directory variable
(let* ((env (getenv "TOOLS_DIR"))
       (env-dir (when env (expand-file-name (concat env "/"))))
       (env-init (and env-dir (expand-file-name "init.el" env-dir)))
       (no-env-dir-default (expand-file-name
                            (concat user-home-directory
                                    "~/")))
       (default-init (expand-file-name ".spacemacs" user-home-directory)))

  (defconst tools-directory
    (cond
     ((and env (file-exists-p env-dir))
      env-dir)
     ((file-exists-p no-env-dir-default)
      no-env-dir-default)
     (t
      nil)))
  "Optional tools directory, which defaults to
    ~/. This setting can be overridden using the
    SPACEMACSDIR environment variable. If neither of these
    directories exist, this variable will be nil.")


(defconst bitbucket-directory (concat sandbox-directory "git/bitbucket.org"))
(defconst drgeb-repos-directory (concat bitbucket-directory "/drgeb/repos"))
(defconst github-directory (concat sandbox-directory "git/github.com"))

(message "bitbucket-directory: `%s" bitbucket-directory)
(message "drgeb-repos-directory: `%s" drgeb-repos-directory)
(message "github-directory: `%s" github-directory)

(defconst org-html-themes-directory (concat github-directory "/fniessen/org-html-themes/setup/theme-readtheorg.setup"))
(defconst flycheck-directory (concat github-directory "/flycheck/flycheck-color-mode-line"))
(defconst spaceline-directory (concat github-directory "/TheBB/spaceline/"))
(defconst udemy-directory (concat bitbucket-directory "/learning/go/udemy/Learn_How_To_Code_Googles_Go.org"))
(defconst reveal-directory (concat drgeb-repos-directory "/development/reveal.js/js/reveal.js"))
(defconst maven-pom-mode-directory (concat github-directory "/m0smith/maven-pom-mode"))
(defconst jdk-directory (concat tools-directory "/java_apps/jdk-8u144-docs-all"))
(defconst grammer-file (concat github-directory "/mmagnus/emacs-grammarly/emacs-grammarly.el"))
(defconst line-comment-banner-directory (concat drgeb-repos-directory "/emacs/packages/line-comment-banner"))
(defconst go-autocomplete-file (concat github-directory "/nsf/gocode/emacs/go-autocomplete.el"))
(message "go-autocomplete-file: `%s" go-autocomplete-file)

(defconst flycheck-color-mode-line-directory (concat github-directory "/flycheck/flycheck-color-mode-line"))
(defconst go-workspace-directory (concat sandbox-directory "go-workspace/bin"))
(defconst org-journal-directory (concat bitbucket-directory "/drgeb/daily/org-journal"))
(defconst org-download-image-directory (concat bitbucket-directory "/drgeb/daily/org-dowload-images"))

;;(require 'org-bullets)
;;(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/"))t)
  (add-to-list 'load-path (concat dotspacemacs-directory "/site-lisp/"))
  (add-to-list 'load-path (concat dotspacemacs-directory "/site-lisp/emacs-ac-ispell"))
  (add-to-list 'load-path (concat dotspacemacs-directory "/site-lisp/org/lisp"))
  (add-to-list 'load-path (concat sandbox-directory "/git/github.com/ZehCnaS34/zonokai-emacs"))

  ;;  (add-to-ilst 'package-archives (cons "local" (concat proto ": (concat drgeb-repos-directory "emacs/packages/line-comment-banner"))) t)

  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))

(add-to-list 'custom-theme-load-path (concat sandbox-directory "/git/bitbucket.org/drgeb/repos/emacs/color-themes/"))
(add-to-list 'custom-theme-load-path (concat sandbox-directory "/git/github.com/ZehCnaS34/zonokai-emacs"))

;;drgeb (load-theme 'taming-mr-arneson t)

(require 'ob-tangle)

;; Use use-package now
(use-package req-package
  :ensure t)

;; Load my org-mode configured settings
(org-babel-load-file (expand-file-name (concat dotspacemacs-directory "/drgeb.org")
                                       dotspacemacs-directory))

;; ;; Load custom.el configuration settings
;;(setq custom-file (concat dotspacemacs-directory "/custom.el"))
;;(load custom-file 'no-error 'no-message)

(setq dotspacemacs-check-for-update nil
      dotspacemacs-elpa-https t
      dotspacemacs-elpa-timeout 5
      dotspacemacs-enable-paste-micro-state t
      dotspacemacs-evil-leader-unicode t
      dotspacemacs-excluded-packages '(vi-tilde-fringe)
      dotspacemacs-folding-method 'evil
      dotspacemacs-fullscreen-at-startup nil
      dotspacemacs-global-visual-line-mode nil
      dotspacemacs-helm-command-prefix-key "C-c h"
      dotspacemacs-helm-resize nil
      dotspacemacs-helm-no-header nil
      dotspacemacs-helm-position 'bottom
      dotspacemacs-helm-use-fuzzy 'always
      dotspacemacs-line-numbers 'relative
      dotspacemacs-retain-visual-state-on-shift t
      dotspacemacs-show-evil-indentation nil
      dotspacemacs-show-transient-state-title nil
      dotspacemacs-undecorate-at-startup nil
      dotspacemacs-whitespace-cleanup 'all
      dotspacemacs-scratch-mode 'org-mode
      dotspacemacs-themes '(leuven spacemacs-dark solarized-light solarized-dark)
      dotspacemacs-colorize-cursor-according-to-state t
      dotspacemacs-default-font '("Fira Code"
                                  :size 14
                                  :weight normal
                                  :width normal
                                  :powerline-scale 1.1)
      dotspacemacs-command-key "SPC"
      dotspacemacs-remap-Y-to-y$ t
      dotspacemacs-visual-line-move-text t
      dotspacemacs-distinguish-gui-tab nil
      dotspacemacs-distinguish-gui-tab-default 'bar
      dotspacemacs-ask-for-lazy-installation nil
      dotspacemacs-verbose-loading nil
      dotspacemacs-startup-banner 'random
      dotspacemacs-scratch-mode 'org-mode
      dotspacemacs-ask-for-lazy-installation nil
      dotspacemacs-configuration-layer-path (list (concat sandbox-directory "git/github.com/"))
      dotspacemacs-configuration-layers
      '((auto-completion :variables
                         auto-completion-enable-snippets-in-popup t
                         auto-completion-enable-help-tooltip t
                         auto-completion-enable-sort-by-usage t
                         auto-completion-enable-snippets-in-popup t
                         auto-completion-enable-help-tooltip t
                         auto-completion-private-snippets-directory (concat sandbox-directory "git/github.com/drgeb/spacemacs-private/snippets")
                         auto-completion-enable-snippets-in-popup t
                         auto-completion-enable-help-tooltip t
                         auto-completion-enable-sort-by-usage t
                         auto-completion-enable-snippets-in-popup t
                         auto-completion-enable-help-tooltip t
                         auto-completion-private-snippets-directory (concat sandbox-directory "git/github.com/drgeb/spacemacs-private/snippets"))
        better-defaults
        command-log
        colors
        elixir
        emacs-lisp
        git
        github
        helm
        helm-spaceline
        html
        java
        javascript
        latex
        markdown
        nginx
        org
        (shell :variables shell-default-height 30
               shell-default-position 'bottom)
        shell-scripts
        spell-checking
        syntax-checking
        themes-megapack
        tmux
        (version-control :variables
                         version-control-diff-tool 'git-gutter+)
        yaml
        (python :variables python-backend 'lsp
                python-lsp-server 'mspyls
                python-lsp-git-root-directory dotspacemacs-configuration-layer-path
                python-lsp-auto-guess-root t)))

(setq dotspacemacs-additional-packages
      '(helm-make helm-dash org-journal journal go-autocomplete
                   neotree web-mode emmet-mode exec-path-from-shell))

;;(setq dotspacemacs-additional-packages '(helm-dash))

(setq custom-file (concat dotspacemacs-directory "custom.el"))

(when (file-exists-p custom-file)
  (load custom-file))

(defun dotspacemacs/init ()
  (add-hook 'after-init-hook 'sml/setup))

(setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc))

(setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc))

(defun dotspacemacs/user-config ()
  (setq python-shell-interpreter "ipython"
        python-shell-interpreter-args "--simple-prompt -i")

  (exec-path-from-shell-initialize)

  (require 'helm-dash)
  (setq helm-dash-browser-func 'eww))

(defun dotspacemacs/user-init ()
  (setq
   inhibit-startup-screen t
   ring-bell-function 'ignore
   initial-major-mode 'text-mode
   initial-scratch-message
   "# This buffer is for notes you don't want to save, and for Lisp evaluation.
# If you want to create a file, visit that file with C-x C-f,
# then enter the text in that file's own buffer.")

  (setq warning-minimum-level :error
        visible-bell nil
        require-final-newline t
        frame-title-format '((:eval (projectile-project-name)))
        save-interprogram-paste-before-kill t
        browse-url-browser-function 'eww-browse-url
        browse-url-generic-program "google-chrome-stable"
        helm-mode-fuzzy-match t
        helm-completion-in-region-fuzzy-match t
        helm-candidate-number-limit 500
        helm-echo-input-in-header-line t
        helm-autoresize-max-height 0
        helm-autoresize-min-height 20
        helm-split-window-in-side-p t
        helm-always-two-windows t
        helm-move-to-line-cycle-in-source t
        helm-ff-search-library-in-sexp t
        helm-M-x-fuzzy-match t
        helm-buffers-fuzzy-matching t
        helm-recentf-fuzzy-match t
        helm-semantic-fuzzy-match t
        helm-imenu-fuzzy-match t
        helm-lisp-fuzzy-completion t
        projectile-enable-caching t
        show-paren-delay 0
        blink-cursor-interval 0.5
        sp-highlight-pair-overlay nil
        sp-highlight-wrap-overlay nil
        sp-highlight-wrap-tag-overlay nil
        sp-show-pair-from-inside t
        sp-cancel-autoskip-on-backward-movement nil
        sp-autoskip-closing-pair 'always
        whitespace-line-column 120
        whitespace-style '(face lines-tail trailing empty indentation::space))

  (add-to-list 'auto-mode-alist '("\\.tex\\'" . latex-mode))
  (add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))

  (setenv "GOPATH" (concat (getenv "SANDBOX_HOME") "/go-workspace"))
  (setenv "PATH" (concat (getenv "PATH") ":/usr/local/go/bin:" (getenv "GOPATH") "/bin"))
  (setq exec-path (append exec-path '("/usr/local/go/bin" (concat (getenv "GOPATH") "/bin")))))

(dotspacemacs/init)

(setq inhibit-startup-screen t)

(provide 'dotspacemacs)
