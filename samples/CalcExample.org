


* Calculation
#+BEGIN_SRC calc
     2 + 3
 #+END_SRC

 #+RESULTS:
 : 5


Issue with ob-calc not loadingcnot sure why configuration is skipping it!
#+BEGIN_SRC emacs-lisp
(load "ob-calc" )
#+END_SRC

#+RESULTS:
: t

Error I am seeing is
if: No org-babel-execute function for ditaa!

* Python: examples obtained from [[http://orgmode.org/worg/org-contrib/babel/languages/ob-doc-python.html][Python Source Code Blocks in Org Mode]] site.
** Example 1 Version
#+begin_src python :results output
import sys
print(sys.version)
#+end_src

#+RESULTS:
: 2.7.10 (default, Feb  7 2017, 00:08:15) 
: [GCC 4.2.1 Compatible Apple LLVM 8.0.0 (clang-800.0.34)]

** Example 2 change python to version 3
#+begin_src emacs-lisp :results none
(setq org-babel-python-command "python3")
#+end_src

#+begin_src python :results output
import sys
print(sys.version)
#+end_src

#+RESULTS:
: 3.6.2 (default, Jul 30 2017, 15:59:32) 
: [GCC 4.2.1 Compatible Apple LLVM 8.1.0 (clang-802.0.42)]
** Example 3 change version back to python 2

#+begin_src emacs-lisp :results none
(setq org-babel-python-command "python2")
#+end_src

#+begin_src python :results output
import sys
print(sys.version)
#+end_src

#+RESULTS: 
: 2.7.13 (default, Jul 30 2017, 11:11:06) 
: [GCC 4.2.1 Compatible Apple LLVM 8.1.0 (clang-802.0.42)]
** Session with value mode!!

#+BEGIN_SRC python :session :results value
# blank lines not OK in indented blocks, and don't use return()
# Source block is passed directly to interactive python;
# value is value of _ at end.
#+begin_src python :session
def foo(x):
  if x>0:
    return x+1
  else:
    return x-1

foo(1)

#+END_SRC

#+RESULTS:
: 2

** Python inlining!!
#+begin_src python :results output
print "Hello, world!"
#+end_src

#+RESULTS:
: Hello, world!

** Plotting

1st inst mathplotlib using pip
else you will get

#+BEGIN_SRC shell
pip install matplotlib
#+END_SRC

#+RESULTS:

or use easy_install
#+BEGIN_SRC shell
easy_install matplotlib
#+END_SRC

2nd create the images directory
#+BEGIN_SRC shell
mkdir -pv images
#+END_SRC

#+begin_src python :results file
import matplotlib, numpy
matplotlib.use('Agg')
import matplotlib.pyplot as plt
fig=plt.figure(figsize=(4,2))
x=numpy.linspace(-15,15)
plt.plot(numpy.sin(x)/x)
fig.tight_layout()
plt.savefig('images/python-matplot-fig.png')
return 'images/python-matplot-fig.png' # return filename to org-mode
#+end_src

#+RESULTS:
[[file:images/python-matplot-fig.png]]

* Kotlin
  #+BEGIN_SRC kotlin
  int i=0;
  System.out.println("i= "+i);
  #+END_SRC
* Javascipt
#+begin_src js :cmd "/usr/bin/osascript -l JavaScript"
app = Application.currentApplication()
app.includeStandardAdditions = true
app.say("Hello")
#+end_src

#+RESULTS:
 No module named matplotlib

* TypeScript
  #+BEGIN_SRC typescript
  console.log("Hello World!");
  #+END_SRC
