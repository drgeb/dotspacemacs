#+OPTIONS: toc:nil num:nil reveal_center:nil timestamp:nil
#+TITLE: My Awesome Presentation
#+AUTHOR: Gerald E. Benett
#+REVEAL_HLEVEL: 2
#+REVEAL_THEME: sky
#+REVEAL_TRANS: fade
#+REVEAL_SPEED: fast
#+REVEAL_MARGIN: 0.0
#+REVEAL_EXTRA_CSS: ./presentation.css
#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/3.0.0/


* Slide 1
  here's some text
  
* Slide 2
** subslide 1
** subslide 2
* Slide 3
  #+ATTR_REVEAL: :frag (roll-in)
  - list item 1
  - list item 2
    | a | b | c |
    |---+---+---|
    | 1 | 2 | 3 |
    | 4 | 5 | 6 |
    |---+---+---|
* slide 4
  #+BEGIN_SRC python
    def f(x):
        return x + 1

    print f(5)
  #+END_SRC

  #+RESULTS:
  : None

* Info
    *** REVEAL_HIGHLIGHT_CSS



<section data-background="image.png">

